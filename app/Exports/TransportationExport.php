<?php

namespace App\Exports;

use App\Models\Transportation;
use Maatwebsite\Excel\Concerns\FromCollection;

class TransportationExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Transportation::all();
    }
}
