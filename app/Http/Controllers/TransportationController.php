<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTransportationRequest;
use App\Http\Requests\UpdateTransportationRequest;
use Illuminate\Http\Request;
use App\Models\Transportation;
use App\Exports\TransportationExport;
use Maatwebsite\Excel\Facades\Excel;
class TransportationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transportation = Transportation::latest()->paginate(10);
        return [
            "status" => 1,
            "data" => $transportation
        ];
    }
    public function export()
	{
		return Excel::download(new TransportationExport, 'data.xlsx');
	}
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */

    public function store(Request $request)
    {
        //
        $request->validate([
            'id_transaction' => 'required',
            'name_driver' => 'required',
        ]);

        $transportation = Transportation::create($request->all());
        return [
            "status" => 1,
            "data" => $transportation
        ];
    }

    /**
     * Display the specified resource.
     */
    public function show(Transportation $transportation)
    {
        //
        return [
            "status" => 1,
            "data" =>$transportation
        ];
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Transportation $transportation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Transportation $transportation)
    {
        //
        $request->validate([
            'status' => 'required'
        ]);
 
        $transportation->update($request->all());
 
        return [
            "status" => 1,
            "data" => $transportation,
            "msg" => "Blog updated successfully"
        ];
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Transportation $transportation)
    {
        //
    }
}
