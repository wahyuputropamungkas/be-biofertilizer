<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_transaction',
        'name_driver',
        'unit',
        'start_hours',
        'end_hours',
        'total_hours',
        'from_location',
        'to_location',
        'requirements_details',
        'output_details',
        'sat_details',
        'convert_details',
        'block_details',
        'location_details',
        'info_details',
        'area_details',
        'status',
        'user_verif',
        'date_verif',
        'user_posting',
        'date_posting'
    ];
}
