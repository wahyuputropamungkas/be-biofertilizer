<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\TransportationController;
use App\Http\Controllers\LoginController;
// use App\Http\Controllers\RegisterController;




// Route::get('register', [RegisterController::class, 'register'])->name('register');
// Route::post('register/action', [RegisterController::class, 'actionregister'])->name('actionregister');
Route::middleware(['cors'])->group(function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::resource('users', UsersController::class);
    Route::resource('transportations', TransportationController::class);
});
// Route::get('/export', 'TransportationController@export');
Route::get('/export',  [TransportationController::class, 'export']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
